# Installation

Download the latest release from the [Tags](https://gitlab.com/predator8bit/flare-demonic-ui/tags) and extract it into your flare mods folder.
Or clone the repo and copy the demonic-ui folder into your mods folder, on Linux:

```sh
git clone https://gitlab.com/predator8bit/flare-demonic-ui.git
cd flare-demonic-ui
cp -Ri demonic-ui $HOME/.local/share/flare/mods/
```

After this, open the game and enable the mod in the Configuration menu under the Mods tab, click ok and enjoy!